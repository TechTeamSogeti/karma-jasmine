describe('Test du userService', function() {
    var service, httpBackend, timeout;

    beforeEach(function() {
        module('myApp.service.user');

        inject(function($httpBackend, $timeout, _userService_) {
            service = _userService_;
            httpBackend = $httpBackend;
            timeout = $timeout;
        });

        httpBackend.whenGET('https://jsonplaceholder.typicode.com/users').respond('users');
        httpBackend.whenGET('https://jsonplaceholder.typicode.com/users/1').respond('user1');

        spyOn(service.$http, 'get').and.callThrough();
    });

    afterEach(function() {
        httpBackend.flush();
    });

    it(' - test de l\'init du service', function() {
        expect(service.$http).toBeDefined();
        expect(service.url).toEqual('https://jsonplaceholder.typicode.com/users');

        expect(service.getUsers).toBeDefined();
        expect(service.getUser).toBeDefined();
    });

    it(' - test de la méthode getUsers', function() {
        service.getUsers().then(function(result) {
            expect(service.$http.get).toHaveBeenCalledWith('https://jsonplaceholder.typicode.com/users');
            expect(result.data).toEqual('users');
        });
    });

    it(' - test de la méthode getUser', function() {
        service.getUser(1).then(function(result) {
            expect(service.$http.get).toHaveBeenCalledWith('https://jsonplaceholder.typicode.com/users/1');
            expect(result.data).toEqual('user1');
        });
    });

});