angular.
module('myApp.service.user', [])
    .service('userService', UserService);

function UserService($http) {
    this.$http = $http;
    this.url = 'https://jsonplaceholder.typicode.com/users';
}

UserService.prototype.getUsers = function () {
    return this.$http.get(this.url);
};

UserService.prototype.getUser = function (id) {
    url = this.url + '/' + id;
    return this.$http.get(url);
};
