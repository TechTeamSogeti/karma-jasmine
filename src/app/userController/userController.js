'use strict';

angular.module('myApp.controller.user', [
    'ngRoute',
    'myApp.service.user'
])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/user/:id', {
            templateUrl: 'userController/userController.html',
            controller: 'userController',
            controllerAs: 'vm'
        });
    }])

    .controller('userController', UserController);

    function UserController($routeParams, userService) {
        this.$routeParams = $routeParams;
        this.userService = userService;

        this.id = this.$routeParams.id;
        this.getUser();
    }

    UserController.prototype.getUser = function () {
        var self = this;

        this.userService.getUser(this.id)
            .then(function(result) {
                self.user = result.data;
            });
    };