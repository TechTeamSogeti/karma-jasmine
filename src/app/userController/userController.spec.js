describe('Test du userController', function() {
    var controller, userService, timeout, $routeParams;

    beforeEach(function() {
        module('myApp.controller.user');

        inject(function($q, $timeout, $controller) {
            timeout = $timeout;

            userService = {
                getUser: function() {
                    return $q.when({data: 'user'});
                }
            };

            $routeParams = {
                id : 1
            }

            controller = $controller('userController', {$routeParams : $routeParams, userService: userService});

            spyOn(controller.userService, 'getUser').and.callThrough();
        });
    });

    it(' - Test de l\'init du controller', function() {
        expect(controller.userService).toBeDefined();
        expect(controller.id).toEqual(1);

        expect(controller.getUser).toBeDefined();
    });

    it(' - Test de la méthode getUSer', function() {
        controller.getUser();

        timeout.flush();

        expect(controller.user).toEqual('user');
    });

})