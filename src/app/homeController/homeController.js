'use strict';

angular.module('myApp.controller.home', [
    'ngRoute',
    'myApp.service.user'
])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider.when('/home', {
            templateUrl: 'homeController/homeController.html',
            controller: 'homeController',
            controllerAs: 'vm'
        });
    }])

    .controller('homeController', HomeController);

    function HomeController($location, userService) {
        this.$location = $location;
        this.userService = userService;
        this.getUsers();
    }

    HomeController.prototype.getUsers = function () {
        var self = this;

        this.userService.getUsers()
            .then(function(result) {
                self.users = result.data;
            });
    };

    HomeController.prototype.details = function (user) {
        if(user.id) {
            this.$location.path('user/' + user.id);
        }else {
            this.$location.path('/');
        }
    };