describe('Test du controller HomeController', function() {
    var controller, timeout;

    beforeEach(function() {
        module('myApp.controller.home');

        inject(function($q, $timeout, $controller) {
            timeout = $timeout;

            var userService = {
                getUsers: function() {
                    return $q.when({data: 'toto'});
                }
            };

            controller = $controller('homeController', {userService: userService});
        });
    });

    it('Test de l\'init de mon controller', function() {
        expect(controller.$location).toBeDefined();
        expect(controller.userService).toBeDefined();
        expect(controller.getUsers).toBeDefined();
        expect(controller.details).toBeDefined();
    });

    describe('Test de la méthode details', function() {

        beforeEach(function() {
            spyOn(controller.$location, 'path');
        });

        it(' - avec un userId', function() {

            var user = {
                id: 1
            };

            controller.details(user);

            expect(controller.$location.path).toHaveBeenCalledWith('user/1');
        });

        it(' - sans un userId', function() {

            var user = {};

            controller.details(user);

            expect(controller.$location.path).toHaveBeenCalledWith('/');
        });
    });

    it('Test de la méthode getUsers', function() {
        controller.getUsers();

        timeout.flush();

        expect(controller.users).toEqual('toto');
    });

});