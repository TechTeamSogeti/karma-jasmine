//jshint strict: false
module.exports = function(config) {
  config.set({

    basePath: './src/app',

    files: [
      'bower_components/angular/angular.js',
      'bower_components/angular-route/angular-route.js',
      'bower_components/angular-mocks/angular-mocks.js',
      '*Controller/*.js',
      'services/*.js'
    ],

    // coverage reporter generates the coverage
    reporters: ['progress', 'coverage'],

    preprocessors: {
        // source files, that you wanna generate coverage for
        // do not include tests or libraries
        // (these files will be instrumented by Istanbul)
        './**/*Controller.js': ['coverage'],
        './**/*Service.js': ['coverage']
    },

    autoWatch: true,

    frameworks: ['jasmine'],

    browsers: ['PhantomJS'],

    plugins: [
      'karma-chrome-launcher',
      'karma-firefox-launcher',
      'karma-jasmine',
      'karma-junit-reporter',
      'karma-phantomjs-launcher',
      'karma-coverage'
    ],

      coverageReporter: {
          includeAllSources: true,
          dir: '../../coverage/',
          reporters: [
              { type: "html" },
              { type: "text" },
              { type: 'text-summary' }
          ]
      },

    junitReporter: {
      outputFile: 'test_out/unit.xml',
      suite: 'unit'
    }

  });
};
